
const path = require("path")


const firstProblem = (fs,obj1,obj2) => {
    fs.mkdir("json", function () {
        fs.writeFile("./json/example1.json", JSON.stringify(obj1), function (err) {
          if (err) throw err;
          console.log("example1.json created.");
          fs.writeFile("./json/example2.json", JSON.stringify(obj2), function (err) {
            if (err) throw err;
            console.log("example2.json created");
            
      
            deleteFiles();
          });
        });
      });
      
      function deleteFiles() {
        const directory = "json";
        fs.readdir(directory, (err, files) => {
          if (err) throw err;
          console.log(files);
          for (const file of files) {
            fs.unlink(path.join(directory, file), (err) => {
              if (err) throw err;
              console.log(`${file} deleted`)
            });
          }
        });
      }
}

module.exports = firstProblem