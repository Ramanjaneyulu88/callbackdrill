function secondProblem(fs, file) {
  //1. Read the given file lipsum.txt

  fs.readFile(file, "utf-8", (error, data) => {
    if (error) throw error;
    const contents = data;
    console.log("read lipsum");
    convertAndWrite(contents);
  });

  //2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
  function convertAndWrite(contents) {
    fs.writeFile("new_file1.txt", contents.toUpperCase(), function (err) {
      if (err) throw err;
      console.log("new_file1 created");
      fs.writeFile("filenames.txt", "new_file1.txt,", function (err) {
        if (err) throw err;
        console.log("new_file1 was added to filenames.1");
      });
      readAndSplit();
    });
  }

  //3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
  function readAndSplit() {
    fs.readFile("new_file1.txt", "utf-8", (error, data) => {
      if (error) throw error;
      let content = data;
      splitAndWrite(content);
    });
  }

  function splitAndWrite(data) {
    const loweredAndSplitData = JSON.stringify(data.toLowerCase().split("."));

    fs.writeFile("new_file2.txt", loweredAndSplitData, function (err) {
      if (err) throw err;
      console.log("new_file2 was created.");
      fs.appendFile("filenames.txt", "new_file2.txt,", function (err) {
        if (err) throw err;
        console.log("new_file2 was appended to filenames");
        readAndSort("new_file2.txt");
      });
    });
  }

  // 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
  function readAndSort(file) {
    fs.readFile(file, "utf-8", (err, data) => {
      if (err) throw err;

      let content = JSON.stringify(JSON.parse(data).sort());
      console.log("Content sorted.");

      fs.writeFile("new_file3.txt", content, function (err) {
        if (err) throw err;
        fs.appendFile("filenames.txt", "new_file3.txt", function (err) {
          if (err) throw err;
          console.log("file_name3.txt was created and appended to filesname");
          deleteFiles();
        });
      });
    });
  }

  //  5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
  function deleteFiles() {
    fs.readFile("filenames.txt", "utf-8", function (err, data) {
      if (err) throw err;
      let content = data.split(",");
      for (let each of content) {
        fs.unlink(each, (err) => {
          if (err) throw err;
          console.log(`${each} deleted`);
        });
      }
    });
  }
}

module.exports = secondProblem;
